/// Enable virtual terminal processing for windows support.
///
/// # Examples
///
/// ```
/// if cfg!(target_os = "windows") {
///    r256::enable_virtual_terminal_processing();
/// }
/// ```
pub fn enable_virtual_terminal_processing() {
    use winapi_util::console::Console;

    if let Ok(mut terminal) = Console::stdout() {
        let _ = terminal.set_virtual_terminal_processing(true);
    }
    if let Ok(mut terminal) = Console::stderr() {
        let _ = terminal.set_virtual_terminal_processing(true);
    }
}