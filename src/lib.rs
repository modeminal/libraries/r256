#[cfg(windows)]
mod virtual_processing;
#[cfg(windows)]
pub use virtual_processing::*;

#[derive(Debug, Eq, PartialEq)]
/// Style enum that containes both color and text utils.
pub enum Styles {
    Bold,
    Italic,
    Faint,
    Underline,
    Strike,
    Invisible,
    Blink,
    Reverse,
    FgColor256(u8),
    BgColor256(u8)
}

/// Initialize the r256 for cross-platform support.
///
/// # Examples
///
/// ```
/// r256::init();
/// ```
pub fn init() {
    #[cfg(windows)]
    enable_virtual_terminal_processing();
}

fn convert_to_string(style: &Styles) -> String {
    match style {
        Styles::Bold => String::from("\x1b[1m"),
        Styles::Italic => String::from("\x1b[3m"),
        Styles::Faint => String::from("\x1b[2m"),
        Styles::Underline => String::from("\x1b[4m"),
        Styles::Strike => String::from("\x1b[9m"),
        Styles::Invisible => String::from("\x1b[8m"),
        Styles::Blink => String::from("\x1b[5m"),
        Styles::Reverse => String::from("\x1b[7m"),
        Styles::FgColor256(color) => format!("\x1b[38;5;{}m", color),
        Styles::BgColor256(color) => format!("\x1b[48;5;{}m", color)
    }
}

/// Generate a string with given styles. 
///
/// # Arguments
/// 
/// * `styles` (`&Vec<Styles>`) - The styles will be appended to the string.
/// * `text` (`&str`) - The text.
/// 
/// # Examples
///
/// ```
/// let mut color_combines: Vec<r256::Styles> = Vec::new();
/// color_combines.push(r256::Styles::FgColor256(1));
/// color_combines.push(r256::Styles::Bold);
/// 
/// let output_str = r256::generate_string(&color_combines, "hello!");
/// println!("{}", output_str);
/// ```
pub fn generate_string(styles: &Vec<Styles>, text: &str) -> String {
    let mut generated_str = String::new();
    
    for style in styles {
        generated_str = format!("{}{}", generated_str, convert_to_string(style));
    }

    generated_str + text + "\x1b[0m"
}

/// Generate a string and print to the stdout. 
///
/// # Arguments
/// 
/// * `styles` (`&Vec<Styles>`) - The styles will be appended to the string.
/// * `text` (`&str`) - The text.
/// 
/// # Examples
///
/// ```
/// let mut color_combines: Vec<r256::Styles> = Vec::new();
/// color_combines.push(r256::Styles::FgColor256(42));
/// color_combines.push(r256::Styles::Bold);
/// 
/// r256::print(&color_combines, "hello!");
/// ```
pub fn print(styles: &Vec<Styles>, text: &str) {
    print!("{}", generate_string(styles, text))
}

/// Generate a string and print with new line to the stdout. 
///
/// # Arguments
/// 
/// * `styles` (`&Vec<Styles>`) - The styles will be appended to the string.
/// * `text` (`&str`) - The text.
/// 
/// # Examples
///
/// ```
/// let mut color_combines: Vec<r256::Styles> = Vec::new();
/// color_combines.push(r256::Styles::FgColor256(69));
/// color_combines.push(r256::Styles::Bold);
/// 
/// r256::println(&color_combines, "hello!");
/// ```
pub fn println(styles: &Vec<Styles>, text: &str) {
    println!("{}", generate_string(styles, text))
}