# r256

Simple cross-platform color 256 library with text styles for Rust.

## Links

- [Online Documentation](https://docs.rs/r256/)
- [Crates.io](https://crates.io/crates/r256)

## Example Code

```rust
use r256;

fn main() {
    r256::init();
    
    for i in 0..256 {
      let mut color_combines: Vec<r256::Styles> = Vec::new();
      color_combines.push(r256::Styles::FgColor256(i as u8));
      color_combines.push(r256::Styles::Bold);

      r256::println(&color_combines, "r256.");
    }
}
```

## Found a Bug / Error?

If you found a bug or an error, please create a new issue at gitlab repository.

## Contributing

If you want to contribute this project:

- Make sure you add the comments for your codes.
- Please do not something useless.

## Authors

- [Modeminal](https://gitlab.com/modeminal)

## License

This project is distributed under MIT license.

## Project status

Under development.
